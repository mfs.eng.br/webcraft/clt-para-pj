# Calculadora CLT x PJ

## Descrição
Este repositório contém o código para uma aplicação web "Calculadora CLT x PJ". Esta ferramenta ajuda usuários a comparar a remuneração entre contratos de trabalho CLT (Consolidação das Leis do Trabalho) e contratos PJ (Pessoa Jurídica) no Brasil, considerando diferentes fatores como FGTS, INSS, 13º salário, férias e outros encargos e benefícios.

## Tecnologias Utilizadas
- HTML
- CSS
- JavaScript

## Configuração e Execução
Para executar este projeto, basta clonar o repositório e abrir o arquivo `index.html` em um navegador web.

```bash
git clone [URL_DO_REPOSITORIO]
cd [NOME_DO_REPOSITORIO]
# Abra o arquivo index.html em seu navegador
```
# Como Usar
- Insira o salário fixo e as informações de comissão para calcular a equivalência de salário entre CLT e PJ.
- Os resultados serão exibidos em tabelas logo abaixo dos campos de entrada.

# Contribuição
Contribuições para o projeto são bem-vindas. Por favor, siga os seguintes passos:

- Faça um fork do repositório.
- Crie uma branch: ``git checkout -b [nome_branch]``.
- Faça suas alterações e commit: ``git commit -m '[mensagem_commit]'``.
- Envie para a branch original: ``git push origin [nome_do_projeto]/[nome_branch]``.
- Crie a pull request.